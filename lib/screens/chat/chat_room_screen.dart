import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:proto_chat/helpers/database_helper.dart';
import 'package:proto_chat/models/message_model.dart';
import 'package:proto_chat/providers/messages_db_provider.dart';
import 'package:proto_chat/providers/user_provider.dart';
import 'package:proto_chat/screens/chat/chat_list_screen.dart';
import 'package:proto_chat/screens/chat/widgets/message_bubble.dart';
import 'package:proto_chat/services/database_service.dart';
import 'package:proto_chat/utils/constants.dart';
import 'package:proto_chat/utils/utils.dart';
import 'package:proto_chat/widgets/custom_app_bar.dart';
import 'package:proto_chat/widgets/custom_loader_widget.dart';
import 'package:provider/provider.dart';

class ChatRoomScreen extends StatelessWidget {
  static String screenId = 'chat_room_screen';

  ChatRoomScreen({@required this.chatroomId});

  final String chatroomId;

  final TextEditingController _messageTextEditingController = TextEditingController();
  final DatabaseService _databaseService = DatabaseService();
  final MessagesDbProvider _messagesDb = MessagesDbProvider();

  void sendMessage(BuildContext context) {
    if (_messageTextEditingController.text.isEmpty) {
      return;
    }
    String loggedInUsername = Provider.of<UserProvider>(context, listen: false).username;
    int timestamp = DateTime.now().millisecondsSinceEpoch;
    Map<String, dynamic> message = {
      'message': _messageTextEditingController.text,
      'sender': loggedInUsername,
      'timestamp': timestamp,
    };
    _databaseService.sendChatMessages(
      chatroomId: chatroomId,
      message: message,
    );
  }

  /// method to convert new messages comming from stream,
  /// start saving them in local database and return list
  /// of MessageModel to the caller.
  List<MessageModel> saveNewMessages(Iterable<QueryDocumentSnapshot> newMessages) {
    List<MessageModel> convertedNewMessages = newMessages
        .map((newMessage) => MessageModel.fromMessage(
              {
                'roomId': chatroomId,
                'messageId': newMessage.id,
                ...newMessage.data(),
              },
            ))
        .toList();
    convertedNewMessages.forEach((message) {
      _messagesDb.addMessage(message);
    });
    return convertedNewMessages;
  }

  @override
  Widget build(BuildContext context) {
    print('ChatRoomScreen build');
    return GestureDetector(
      onTap: () => unfocusKeyboard(context),
      child: Scaffold(
        appBar: CustomAppBar(
          leading: BackButton(
            onPressed: () {
              DatabaseHelper.saveMessageTimestamp(
                chatroomId: chatroomId,
                timestamp: DateTime.now().millisecondsSinceEpoch,
              );
              // saveNewMessages(_messages);
              // delete all the route
              // it deleates the route because there are two ways it can come to the chat room
              // 1. ChatListScreen
              // 2. SearchListScreen
              Navigator.pushNamedAndRemoveUntil(context, ChatListScreen.screenId, (route) => false);
            },
          ),
        ),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              MessagePage(
                chatroomId: chatroomId,
              ),
              Container(
                decoration: kMessageContainerDecoration,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    IconButton(icon: Icon(Icons.add), onPressed: () => {}),
                    Expanded(
                      child: TextField(
                        style: TextStyle(
                          color: Colors.white,
                        ),
                        controller: _messageTextEditingController,
                        decoration: kMessageTextFieldDecoration,
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        sendMessage(context);
                        _messageTextEditingController.clear();
                        unfocusKeyboard(context);
                        // send message to firebase
                      },
                      child: Text(
                        'Send',
                        style: kSendButtonTextStyle,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MessagePage extends StatelessWidget {
  MessagePage({
    @required this.chatroomId,
  });

  final String chatroomId;

  @override
  Widget build(BuildContext context) {
    return LocalDBFuture(
      chatroomId: chatroomId,
    );
  }
}

class LocalDBFuture extends StatelessWidget {
  LocalDBFuture({
    @required this.chatroomId,
  });

  final String chatroomId;
  final MessagesDbProvider _messagesDb = MessagesDbProvider();

  @override
  Widget build(BuildContext context) {
    print('LocalDBFuture build');
    return FutureBuilder(
      future: _messagesDb.fetchMessage(chatroomId: chatroomId),
      builder: (BuildContext context, AsyncSnapshot<List<MessageModel>> snapshot) {
        if (snapshot.hasError) {
          return Container();
        }
        return MessageStream(
          chatroomId: chatroomId,
          localMessages: snapshot.data,
        );
      },
    );
  }
}

class MessageStream extends StatelessWidget {
  MessageStream({
    @required this.chatroomId,
    this.localMessages,
  });

  final String chatroomId;
  final List<MessageModel> localMessages;
  final MessagesDbProvider _messagesDb = MessagesDbProvider();
  final DatabaseService _databaseService = DatabaseService();

  /// method to convert new messages comming from stream,
  /// start saving them in local database and return list
  /// of MessageModel to the caller.
  void saveNewMessages(List<MessageModel> newMessages) {
    newMessages.forEach((message) {
      _messagesDb.addMessage(message);
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _databaseService.getChatMessage(chatroomId: chatroomId),
      builder: (BuildContext context, AsyncSnapshot<Stream<QuerySnapshot>> snapshot) {
        if (snapshot.hasError) {
          return Container();
        }
        if (!snapshot.hasData) {
          return CustomLoaderWidget();
        }
        return StreamBuilder(
          stream: snapshot.data,
          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
            if (streamSnapshot.hasError) {
              return Container();
            }
            if (!streamSnapshot.hasData) {
              return CustomLoaderWidget();
            }
            print('StreamBuilder');
            final streamMessages = streamSnapshot.data.docs;
            // after the data is fetched, save the current timestamp in millisecondsSinceEpoch
            // using the chatroomId as key of the value.
            // convert messages
            List<MessageModel> messages = [];
            if (streamMessages.isNotEmpty) {
              messages = streamMessages
                  .map((newMessage) => MessageModel.fromMessage(
                        {
                          'roomId': chatroomId,
                          'messageId': newMessage.id,
                          ...newMessage.data(),
                        },
                      ))
                  .toList();
            }
            print('messages: $messages');
            print('localMessages: $localMessages');
            // if messages is not null and not empty, save messages to local database.
            if (messages != null && messages.isNotEmpty) {
              saveNewMessages(messages);
            }
            if (localMessages != null) {
              messages = [...messages, ...localMessages];
            }
            return Expanded(
              child: ListView(
                reverse: true,
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
                children: messages.reversed.map((message) {
                  String loggedInUsername =
                      Provider.of<UserProvider>(context, listen: false).username;
                  final String messageSender = message.sender;
                  final bool isCurrentSender = messageSender == loggedInUsername;
                  return MessageBubble(
                    use: message,
                    isCurrentUser: isCurrentSender,
                  );
                }).toList(),
              ),
            );
          },
        );
      },
    );
  }
}
