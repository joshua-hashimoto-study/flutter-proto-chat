import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:proto_chat/providers/user_provider.dart';
import 'package:proto_chat/screens/chat/chat_room_screen.dart';
import 'package:proto_chat/services/database_service.dart';
import 'package:proto_chat/utils/constants.dart';
import 'package:proto_chat/widgets/custom_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class SearchUserScreen extends StatefulWidget {
  static String screenId = 'search_user_screen';

  @override
  _SearchUserScreenState createState() => _SearchUserScreenState();
}

class _SearchUserScreenState extends State<SearchUserScreen> {
  TextEditingController _searchTextEditingController = TextEditingController();
  DatabaseService _databaseService = DatabaseService();
  QuerySnapshot searchSnapshot;

  void searchUser() async {
    QuerySnapshot snapshot = await _databaseService.getUsersByUsername(
      username: _searchTextEditingController.text,
    );
    setState(() {
      searchSnapshot = snapshot;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Container(
        child: Column(
          children: [
            Container(
              color: Color(0x54FFFFFF),
              padding: const EdgeInsets.symmetric(
                horizontal: 24.0,
                vertical: 16.0,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: _searchTextEditingController,
                      style: kAuthTextFieldTextStyle,
                      decoration: kAuthTextFieldDecoration.copyWith(
                        hintText: 'search users...',
                      ),
                    ),
                  ),
                  IconButton(
                    color: Colors.grey.shade300,
                    icon: Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                    onPressed: searchUser,
                  ),
                ],
              ),
            ),
            searchList(),
          ],
        ),
      ),
    );
  }

  Widget searchList() {
    if (searchSnapshot == null) {
      return Container();
    }
    List<QueryDocumentSnapshot> users = searchSnapshot.docs;
    return Expanded(
      child: ListView.builder(
        itemCount: users.length,
        itemBuilder: (BuildContext context, int index) {
          return SearchItemTile(
            username: users[index].data()['username'],
            email: users[index].data()['email'],
          );
        },
      ),
    );
  }
}

class SearchItemTile extends StatelessWidget {
  SearchItemTile({
    @required this.username,
    @required this.email,
  });

  final String username;
  final String email;

  final DatabaseService _databaseService = DatabaseService();

  String getChatRoomId(String a, String b) {
    if (a.substring(0, 1).codeUnitAt(0) > b.substring(0, 1).codeUnitAt(0)) {
      return "$b\_$a";
    } else {
      return "$a\_$b";
    }
  }

  createChatRoomAndStartConversation(BuildContext context, {String username}) async {
    String loggedInUsername = Provider.of<UserProvider>(context, listen: false).username;
    // 1. create chat room
    if (loggedInUsername == null) {
      return;
    }
    // if the searching username is yours then skip the room making
    String roomName = '';
    if (username == loggedInUsername) {
      roomName = 'My room';
    }
    List<String> users = [loggedInUsername, username];
    String chatroomId = Uuid().v4();
    Map<String, dynamic> roomData = {
      'users': users,
      'roomId': chatroomId,
      'roomName': roomName,
      'lastUpdate': Timestamp.now(),
    };
    // 2. send user to char room screen(maybe with pushReplacementName)
    await _databaseService.createChatRoom(
      roomId: chatroomId,
      chatroomData: roomData,
    );
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => ChatRoomScreen(
          chatroomId: chatroomId,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 24.0,
        vertical: 16.0,
      ),
      child: Row(
        children: [
          Column(
            children: [
              Text(
                username,
                style: kAuthTextFieldTextStyle,
              ),
              Text(
                email,
                style: kAuthTextFieldTextStyle,
              ),
            ],
          ),
          Spacer(),
          GestureDetector(
            onTap: () => createChatRoomAndStartConversation(context, username: username),
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: 16.0,
                vertical: 8.0,
              ),
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Text(
                'Message',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
