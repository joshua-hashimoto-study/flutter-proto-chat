import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:proto_chat/helpers/database_helper.dart';
import 'package:proto_chat/providers/user_provider.dart';
import 'package:proto_chat/screens/chat/chat_room_screen.dart';
import 'package:proto_chat/screens/chat/search_user_screen.dart';
import 'package:proto_chat/screens/welcome_screen.dart';
import 'package:proto_chat/services/auth_service.dart';
import 'package:proto_chat/services/database_service.dart';
import 'package:proto_chat/utils/constants.dart';
import 'package:proto_chat/widgets/custom_app_bar.dart';
import 'package:proto_chat/widgets/custom_loader_widget.dart';
import 'package:provider/provider.dart';

class ChatListScreen extends StatefulWidget {
  static const String screenId = 'chat_list_screen';
  @override
  _ChatListScreenState createState() => _ChatListScreenState();
}

class _ChatListScreenState extends State<ChatListScreen> {
  AuthService _authService = AuthService();

  @override
  void initState() {
    setUserInfo();
    super.initState();
  }

  Future<void> setUserInfo() async {
    String username = await DatabaseHelper.fetchUserName();
    Provider.of<UserProvider>(context, listen: false).updateUsername(username);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        actions: [
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              _authService.signout();
              DatabaseHelper.deleteLoggedInData();
              Navigator.pushReplacementNamed(
                context,
                WelcomeScreen.screenId,
              );
            },
          ),
        ],
      ),
      body: ChatroomStream(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.pushNamed(context, SearchUserScreen.screenId);
        },
      ),
    );
  }
}

class ChatroomStream extends StatelessWidget {
  final DatabaseService _databaseService = DatabaseService();

  @override
  Widget build(BuildContext context) {
    String loggedInUsername = Provider.of<UserProvider>(context).username;
    return StreamBuilder(
      stream: _databaseService.getChats(username: loggedInUsername),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) {
          return CustomLoaderWidget();
        }
        final chats = snapshot.data.docs;
        if (chats.length == 0) {
          return Center(
            child: Text(
              'no one to chat with.',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          );
        }
        return ListView.builder(
          itemCount: chats.length,
          itemBuilder: (BuildContext context, int index) {
            Map<String, dynamic> chat = chats[index].data();
            String loggedInUsername = Provider.of<UserProvider>(context, listen: false).username;
            String roomName = chat['roomName'];
            String username = roomName == null || roomName.isEmpty
                ? chat['users'].where((user) => user != loggedInUsername).toList()[0]
                : roomName;
            String chatroomId = chat['roomId'];
            return ChatroomTile(chatroomId: chatroomId, username: username);
          },
        );
      },
    );
  }
}

class ChatroomTile extends StatelessWidget {
  const ChatroomTile({@required this.chatroomId, @required this.username});

  final String chatroomId;
  final String username;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 24.0,
        vertical: 16.0,
      ),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => ChatRoomScreen(
                chatroomId: chatroomId,
              ),
            ),
          );
        },
        child: Row(
          children: [
            Container(
              height: 40.0,
              width: 40.0,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(
                  40.0,
                ),
              ),
              child: Text(
                "${username.substring(0, 1).toUpperCase()}",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(
              width: 8.0,
            ),
            Text(
              username,
              style: kAuthTextFieldTextStyle,
            ),
          ],
        ),
      ),
    );
  }
}
