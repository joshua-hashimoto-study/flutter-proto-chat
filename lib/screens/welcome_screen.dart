import 'package:flutter/material.dart';
import 'package:proto_chat/helpers/database_helper.dart';
import 'package:proto_chat/providers/messages_db_provider.dart';
import 'package:proto_chat/providers/user_provider.dart';
import 'package:proto_chat/screens/auth/signin_screen.dart';
import 'package:proto_chat/screens/auth/signup_screen.dart';
import 'package:proto_chat/screens/chat/chat_list_screen.dart';
import 'package:proto_chat/widgets/custom_dialog.dart';
import 'package:provider/provider.dart';

class WelcomeScreen extends StatefulWidget {
  static const String screenId = 'welcome_screen';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> with SingleTickerProviderStateMixin {
  bool _showSignin = true;

  void toggleView() {
    setState(() {
      _showSignin = !_showSignin;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: DatabaseHelper.fetchUserLoggedIn(),
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        // if theres an error in the future value
        // then show the error dialog(hopefully).
        if (snapshot.hasError) {
          showErrorDialog(context, errorMessage: 'something went wrong');
          return Scaffold();
        }
        if (snapshot.data == null || snapshot.data == false) {
          if (_showSignin) {
            return SigninScreen(
              onToRegister: toggleView,
            );
          } else {
            return SignupScreen(
              onToLogin: toggleView,
            );
          }
        } else {
          return ChatListScreen();
        }
      },
    );
  }
}
