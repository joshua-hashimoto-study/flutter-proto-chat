import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:proto_chat/helpers/database_helper.dart';
import 'package:proto_chat/screens/auth/widgets/auth_rounded_button_widget.dart';
import 'package:proto_chat/screens/chat/chat_list_screen.dart';
import 'package:proto_chat/services/auth_service.dart';
import 'package:proto_chat/services/database_service.dart';
import 'package:proto_chat/utils/constants.dart';
import 'package:proto_chat/widgets/custom_app_bar.dart';
import 'package:proto_chat/widgets/custom_dialog.dart';
import 'package:proto_chat/models/user_model.dart' as user_model;

class SigninScreen extends StatefulWidget {
  SigninScreen({this.onToRegister});

  static String screenId = 'signin_screen';

  final Function onToRegister;

  @override
  _SigninScreenState createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _emailTextEditingController = TextEditingController();
  TextEditingController _passwordTextEditingController = TextEditingController();

  bool _isLoading = false;
  AuthService _authService = AuthService();
  DatabaseService _databaseService = DatabaseService();

  void signin() async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    // show loader to user while firebase is proccessing
    setState(() {
      _isLoading = true;
    });
    // signin user using firebase
    user_model.User signinResult = await _authService.signinWithEmailAndPassword(
      email: _emailTextEditingController.text,
      password: _passwordTextEditingController.text,
    );
    // if signup fails then show error dialog
    if (signinResult == null) {
      showErrorDialog(context,
          errorMessage: "Something went wrong while signing you in!\nPlease try again.");
      return;
    }
    // get loggedin user from firebase document collection 'users'
    QuerySnapshot userSnapshot =
        await _databaseService.getCurrentUserByEmail(email: _emailTextEditingController.text);
    // by using the email to get the users list, we know there is only one user so get that.
    Map<String, dynamic> user = userSnapshot.docs[0].data();
    // save to local storage
    DatabaseHelper.saveUserLoggedIn(isUserLoggedIn: true);
    DatabaseHelper.saveUserName(username: user['username']);
    DatabaseHelper.saveUserEmail(email: user['email']);
    // signup success
    Navigator.pushReplacementNamed(context, ChatListScreen.screenId);
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: Container(
          alignment: Alignment.bottomCenter,
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _buildForm(),
                  SizedBox(
                    height: 8.0,
                  ),
                  _buildForgotPassword(),
                  SizedBox(
                    height: 8.0,
                  ),
                  _buildSigninButton(),
                  SizedBox(
                    height: 16.0,
                  ),
                  _buildSigninWithGoogleButton(),
                  SizedBox(
                    height: 16.0,
                  ),
                  _buildRegisterText(),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.1,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          _buildEmailTextField(),
          _buildPasswordTextField(),
        ],
      ),
    );
  }

  Widget _buildEmailTextField() {
    return TextFormField(
      controller: _emailTextEditingController,
      keyboardType: TextInputType.emailAddress,
      style: kAuthTextFieldTextStyle.copyWith(),
      decoration: kAuthTextFieldDecoration.copyWith(
        hintText: 'email',
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return "email is required";
        }
        if (!RegExp(r"^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
            .hasMatch(value)) {
          return "please enter an email address";
        }
        return null;
      },
    );
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      obscureText: true,
      controller: _passwordTextEditingController,
      style: kAuthTextFieldTextStyle.copyWith(),
      decoration: kAuthTextFieldDecoration.copyWith(
        hintText: 'password',
      ),
      validator: (String value) {
        if (value.length < 6) {
          return "password: minimun 6 charactors";
        }
        return null;
      },
    );
  }

  Widget _buildForgotPassword() {
    // Containerでもcenterにできることのいい例
    return Container(
      alignment: Alignment.centerRight,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 16.0,
          vertical: 8.0,
        ),
        child: Text(
          'Forgot Password?',
          style: kAuthTextFieldTextStyle,
        ),
      ),
    );
  }

  Widget _buildSigninButton() {
    return AuthRoundedButtonWidget(
      title: 'Sign In',
      gradients: [
        const Color(0xff007EF4),
        const Color(0xff2A75BC),
      ],
      onPressed: signin,
    );
  }

  Widget _buildSigninWithGoogleButton() {
    return AuthRoundedButtonWidget(
      title: 'Sign In with Google',
      textColor: Colors.black,
      onPressed: () => {},
    );
  }

  Widget _buildRegisterText() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Don't have an account?",
          style: TextStyle(
            color: Colors.white,
            fontSize: 17.0,
          ),
        ),
        GestureDetector(
          onTap: widget.onToRegister,
          child: Container(
            child: Text(
              'Rigster now',
              style: TextStyle(
                color: Colors.white,
                fontSize: 17.0,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
