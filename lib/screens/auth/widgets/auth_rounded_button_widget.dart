import 'package:flutter/material.dart';

class AuthRoundedButtonWidget extends StatelessWidget {
  const AuthRoundedButtonWidget({
    @required this.title,
    @required this.onPressed,
    this.textColor = Colors.white,
    this.gradients = const [
      Colors.white,
      Colors.white,
    ],
  });

  final String title;
  final Function onPressed;
  final Color textColor;
  final List<Color> gradients;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(vertical: 20.0),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: gradients,
          ),
          borderRadius: BorderRadius.circular(30.0),
        ),
        child: Text(
          title,
          style: TextStyle(
            color: textColor,
            fontSize: 17.0,
          ),
        ),
      ),
    );
  }
}
