import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:proto_chat/helpers/database_helper.dart';
import 'package:proto_chat/screens/auth/widgets/auth_rounded_button_widget.dart';
import 'package:proto_chat/screens/chat/chat_list_screen.dart';
import 'package:proto_chat/services/auth_service.dart';
import 'package:proto_chat/services/database_service.dart';
import 'package:proto_chat/utils/constants.dart';
import 'package:proto_chat/widgets/custom_app_bar.dart';
import 'package:proto_chat/widgets/custom_dialog.dart';
import 'package:proto_chat/models/user_model.dart' as user_model;

class SignupScreen extends StatefulWidget {
  SignupScreen({this.onToLogin});

  static String screenId = 'signup_screen';

  final Function onToLogin;

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _usernameTextEditingController = TextEditingController();
  TextEditingController _emailTextEditingController = TextEditingController();
  TextEditingController _passwordTextEditingController = TextEditingController();

  bool _isLoading = false;
  AuthService _authService = AuthService();
  DatabaseService _databaseService = DatabaseService();

  void signup() async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    // show loader to user while firebase is proccessing
    setState(() {
      _isLoading = true;
    });
    user_model.User signupResult = await _authService.signupWithEmailAndPassword(
      email: _emailTextEditingController.text,
      password: _passwordTextEditingController.text,
    );
    if (signupResult == null) {
      showErrorDialog(context,
          errorMessage: "Something went wrong while signing you up!\nPlease try again.");
      return;
    }
    // signup success
    // create user data in firebase
    await _databaseService.uploadUserInfo(
      uid: signupResult.userId,
      email: _emailTextEditingController.text,
      username: _usernameTextEditingController.text,
    );
    // save to local storage
    DatabaseHelper.saveUserLoggedIn(isUserLoggedIn: true);
    DatabaseHelper.saveUserName(username: _usernameTextEditingController.text);
    DatabaseHelper.saveUserEmail(email: _emailTextEditingController.text);
    // navigate to chat list screen
    Navigator.pushReplacementNamed(context, ChatListScreen.screenId);
    // turn off loading
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: Container(
          alignment: Alignment.bottomCenter,
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _buildForm(),
                  SizedBox(
                    height: 8.0,
                  ),
                  // _buildForgotPassword(),
                  // SizedBox(
                  //   height: 8.0,
                  // ),
                  SizedBox(
                    height: 16.0,
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  _buildSignupButton(),
                  SizedBox(
                    height: 16.0,
                  ),
                  _buildSignupWithGoogleButton(),
                  SizedBox(
                    height: 16.0,
                  ),
                  _buildSignupText(),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.1,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          _buildUsernameTextField(),
          _buildEmailTextField(),
          _buildPasswordTextField(),
        ],
      ),
    );
  }

  Widget _buildUsernameTextField() {
    return TextFormField(
      controller: _usernameTextEditingController,
      style: kAuthTextFieldTextStyle.copyWith(),
      decoration: kAuthTextFieldDecoration.copyWith(
        hintText: 'username',
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return "username must be provided";
        } else if (value.length < 4) {
          return "username must be longer then 3 charactors";
        }
        return null;
      },
    );
  }

  Widget _buildEmailTextField() {
    return TextFormField(
      controller: _emailTextEditingController,
      keyboardType: TextInputType.emailAddress,
      style: kAuthTextFieldTextStyle.copyWith(),
      decoration: kAuthTextFieldDecoration.copyWith(
        hintText: 'email',
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return "email is required";
        }
        if (!RegExp(r"^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
            .hasMatch(value)) {
          return "please enter an email address";
        }
        return null;
      },
    );
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      obscureText: true,
      controller: _passwordTextEditingController,
      style: kAuthTextFieldTextStyle.copyWith(),
      decoration: kAuthTextFieldDecoration.copyWith(
        hintText: 'password',
      ),
      validator: (String value) {
        if (value.length < 6) {
          return "password: minimun 6 charactors";
        }
        return null;
      },
    );
  }

  // Widget _buildForgotPassword() {
  //   // Containerでもcenterにできることのいい例
  //   return Container(
  //     alignment: Alignment.centerRight,
  //     child: Container(
  //       padding: EdgeInsets.symmetric(
  //         horizontal: 16.0,
  //         vertical: 8.0,
  //       ),
  //       child: Text(
  //         'Forgot Password?',
  //         style: kAuthTextFieldTextStyle,
  //       ),
  //     ),
  //   );
  // }

  Widget _buildSignupButton() {
    return AuthRoundedButtonWidget(
      title: 'Sign Up',
      gradients: [
        const Color(0xff007EF4),
        const Color(0xff2A75BC),
      ],
      onPressed: signup,
    );
  }

  Widget _buildSignupWithGoogleButton() {
    return AuthRoundedButtonWidget(
      title: 'Sign Up with Google',
      textColor: Colors.black,
      onPressed: () => {},
    );
  }

  Widget _buildSignupText() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Already have an account?",
          style: TextStyle(
            color: Colors.white,
            fontSize: 17.0,
          ),
        ),
        GestureDetector(
          onTap: widget.onToLogin,
          child: Container(
            child: Text(
              'Sign In now',
              style: TextStyle(
                color: Colors.white,
                fontSize: 17.0,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
