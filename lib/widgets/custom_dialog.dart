import 'package:flutter/material.dart';
import 'package:proto_chat/models/alert_button_model.dart';

void showCustomDialog(BuildContext context,
    {String title, String message, List<AlertButton> actions}) {
  List<Widget> _actionWidgets = [
    for (var i = 0; i < actions.length; i++)
      CustomAlertButton(
        title: actions[i].title,
        onPressed: actions[i].onPressed,
      )
  ];
  showDialog(
    context: context,
    builder: (BuildContext context) => AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: _actionWidgets,
    ),
  );
}

void showErrorDialog(BuildContext context, {String errorMessage}) {
  showDialog(
    context: context,
    builder: (BuildContext context) => AlertDialog(
      title: Text('お知らせ'),
      content: Text(errorMessage),
      actions: [
        FlatButton(
          child: Text('OK'),
          onPressed: () {
            return Navigator.pop(context);
          },
        ),
      ],
    ),
  );
}

class CustomAlertButton extends StatelessWidget {
  const CustomAlertButton({
    @required this.title,
    @required this.onPressed,
  });

  final String title;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text(title),
      onPressed: onPressed,
    );
  }
}
