class AlertButton {
  AlertButton({this.title, this.onPressed});

  String title;
  Function onPressed;
}
