import 'package:cloud_firestore/cloud_firestore.dart';

class MessageModel {
  String roomId;
  String messageId;
  String message;
  String sender;
  int timestamp;

  MessageModel.fromMessage(Map<String, dynamic> messageItem)
      : roomId = messageItem['roomId'],
        messageId = messageItem['messageId'],
        message = messageItem['message'],
        sender = messageItem['sender'],
        timestamp = messageItem['timestamp'];

  MessageModel.fromDB(Map<String, dynamic> dbItem)
      : roomId = dbItem['roomId'],
        messageId = dbItem['messageId'],
        message = dbItem['message'],
        sender = dbItem['sender'],
        timestamp = dbItem['timestamp'];

  Map<String, dynamic> toMapFromDb() {
    Map<String, dynamic> map = {
      'roomId': roomId,
      'messageId': messageId,
      'message': message,
      'sender': sender,
      'timestamp': timestamp,
    };
    return map;
  }
}
