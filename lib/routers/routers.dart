import 'package:flutter/material.dart';
import 'package:proto_chat/screens/auth/signin_screen.dart';
import 'package:proto_chat/screens/auth/signup_screen.dart';
import 'package:proto_chat/screens/chat/chat_list_screen.dart';
import 'package:proto_chat/screens/chat/chat_room_screen.dart';
import 'package:proto_chat/screens/chat/search_user_screen.dart';
import 'package:proto_chat/screens/welcome_screen.dart';

String initialRoute = WelcomeScreen.screenId;

Map<String, Widget Function(BuildContext)> routes = {
  WelcomeScreen.screenId: (context) => WelcomeScreen(),
  SigninScreen.screenId: (context) => SigninScreen(),
  SignupScreen.screenId: (context) => SignupScreen(),
  ChatListScreen.screenId: (context) => ChatListScreen(),
  ChatRoomScreen.screenId: (context) => ChatRoomScreen(),
  SearchUserScreen.screenId: (context) => SearchUserScreen(),
};
