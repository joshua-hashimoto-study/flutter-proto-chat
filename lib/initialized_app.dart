import 'package:flutter/material.dart';
import 'package:proto_chat/providers/messages_db_provider.dart';
import 'package:proto_chat/providers/user_provider.dart';
import 'package:proto_chat/routers/routers.dart';
import 'package:proto_chat/screens/welcome_screen.dart';
import 'package:proto_chat/utils/constants.dart';
import 'package:provider/provider.dart';

class InitializedApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => UserProvider(),
        ),
        // Provider<MessagesDbProvider>(
        //   create: (_) => MessagesDbProvider(),
        // ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: Color(0xff145C9E),
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          scaffoldBackgroundColor: kScaffoldBackgroundColor,
        ),
        // initialRoute: initialRoute,
        routes: routes,
        debugShowCheckedModeBanner: true,
        home: GestureDetector(
          onTap: () {
            final FocusScopeNode currentScope = FocusScope.of(context);
            if (!currentScope.hasPrimaryFocus && currentScope.hasFocus) {
              FocusManager.instance.primaryFocus.unfocus();
            }
          },
          child: WelcomeScreen(),
        ),
      ),
    );
  }
}
