import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:proto_chat/initialized_app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  return runApp(InitializedApp());
}
