import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:proto_chat/models/user_model.dart' as user_model;

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  user_model.User _userFromFirebaseUser(User user) {
    return user != null ? user_model.User(userId: user.uid) : null;
  }

  Future signinWithEmailAndPassword({String email, String password}) async {
    try {
      UserCredential signinResult =
          await _auth.signInWithEmailAndPassword(email: email, password: password);
      User firebaseUser = signinResult.user;
      return _userFromFirebaseUser(firebaseUser);
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  Future<user_model.User> signupWithEmailAndPassword({String email, String password}) async {
    try {
      UserCredential signupResult = await _auth
          .createUserWithEmailAndPassword(email: email, password: password)
          .catchError((error) => null);
      User firebaseUser = signupResult.user;
      return _userFromFirebaseUser(firebaseUser);
    } catch (error) {
      print(error);
      return null;
    }
  }

  Future resetPassword({String email}) async {
    try {
      return await _auth.sendPasswordResetEmail(email: email);
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  Future signout() async {
    try {
      return await _auth.signOut();
    } catch (error) {
      print(error.toString());
      return null;
    }
  }
}
