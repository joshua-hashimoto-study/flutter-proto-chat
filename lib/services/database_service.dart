import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:proto_chat/helpers/database_helper.dart';
import 'package:proto_chat/models/message_model.dart';
import 'package:proto_chat/providers/messages_db_provider.dart';
import 'package:uuid/uuid.dart';

class DatabaseService {
  MessagesDbProvider _messagesDB = MessagesDbProvider();
  // collections
  final CollectionReference userCollection = FirebaseFirestore.instance.collection('users');
  final CollectionReference chatroomCollection = FirebaseFirestore.instance.collection('chatrooms');

  Future<QuerySnapshot> getUsersByUsername({String username}) async {
    return await userCollection.where("username", isEqualTo: username).get();
  }

  Future<QuerySnapshot> getCurrentUserByEmail({String email}) async {
    return await userCollection.where("email", isEqualTo: email).get();
  }

  Future uploadUserInfo({String uid, String, email, String username}) async {
    return await userCollection.doc(uid).set({
      'email': email,
      'username': username,
    }).catchError((err) {
      print(err);
      return null;
    });
  }

  Future createChatRoom({String roomId, Map<String, dynamic> chatroomData}) async {
    return await chatroomCollection.doc(roomId).set(chatroomData).catchError((error) {
      print(error);
      return null;
    });
  }

  Future sendChatMessages({String chatroomId, Map<String, dynamic> message}) async {
    final String uuid = Uuid().v4();
    try {
      chatroomCollection.doc(chatroomId).collection("chats").doc(uuid).set(message);
      _messagesDB.addMessage(MessageModel.fromMessage({
        'roomId': chatroomId,
        'messageId': uuid,
        ...message,
      }));
    } catch (error) {
      print(error);
    }
  }

  Future<Stream<QuerySnapshot>> getChatMessage({String chatroomId}) async {
    try {
      CollectionReference chatsCollection = chatroomCollection.doc(chatroomId).collection('chats');
      // get the saved timestamp for the chatroom.
      // if this does not exist then give the current timestamp with millisecondsSinceEpoch
      final int milliseconds = await DatabaseHelper.fetchMessageTimestamp(chatroomId: chatroomId);
      // fetch data from firestore that is create after the timestamp.
      // by doing this, document read should be reduced to minimum(theoretically)
      Stream<QuerySnapshot> messages;
      if (milliseconds != null) {
        messages = chatsCollection
            .where(
              'timestamp',
              isGreaterThan: milliseconds,
            )
            .orderBy('timestamp')
            .snapshots();
      } else {
        messages = chatsCollection.orderBy('timestamp').snapshots();
      }
      // return the new messages
      return messages;
    } catch (error) {
      print(error);
      return null;
    }
  }

  Stream<QuerySnapshot> streamChatMessage({String chatroomId}) {
    try {
      CollectionReference chatsCollection = chatroomCollection.doc(chatroomId).collection('chats');
      Stream<QuerySnapshot> messages = chatsCollection.orderBy('timestamp').snapshots();
      // return the new messages
      return messages;
    } catch (error) {
      print(error);
      return null;
    }
  }

  Stream<QuerySnapshot> getChats({String username}) {
    try {
      return chatroomCollection.where('users', arrayContains: username).snapshots();
    } catch (error) {
      print(error);
      return null;
    }
  }
}
