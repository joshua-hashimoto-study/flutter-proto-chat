import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:proto_chat/models/message_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class MessagesDbProvider {
  static final String tableName = 'Messages';
  MessagesDbProvider._();
  static final MessagesDbProvider _instance = MessagesDbProvider._();
  static Database _database;

  factory MessagesDbProvider() {
    return _instance;
  }

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await init();
    return _database;
  }

  init() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final dbPath = join(documentsDirectory.path, 'messages.db');
    return await openDatabase(
      dbPath,
      version: 1,
      onCreate: (Database newDb, int version) {
        String sql = """
        CREATE TABLE $tableName (
          messageId TEXT PRIMARY KEY,
          roomId TEXT,
          message TEXT,
          sender TEXT,
          timestamp INTEGER
        );
        """;
        newDb.execute(sql);
      },
    );
  }

  Future<int> addMessage(MessageModel message) async {
    final Database db = await database;
    return await db.insert(
      tableName,
      message.toMapFromDb(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<List<MessageModel>> fetchMessage({String chatroomId}) async {
    final Database db = await database;
    final List<Map<String, dynamic>> messages = await db.query(
      tableName,
      columns: null,
      where: "roomId = ?",
      whereArgs: [chatroomId],
    );
    if (messages.length > 0) {
      return messages.map((message) => MessageModel.fromDB(message)).toList();
    }
    return null;
  }
}
