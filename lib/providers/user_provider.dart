import 'package:flutter/material.dart';

class UserProvider extends ChangeNotifier {
  String _username = '';

  String get username => _username;

  void updateUsername(String username) {
    _username = username;
    notifyListeners();
  }
}
