import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DatabaseHelper {
  static String sharedPreferenceUserLoggedInKey = 'IS_LOGGED_IN';
  static String sharedPreferenceUserNameKey = 'USERNAME_KEY';
  static String sharedPreferenceUserEmailKey = 'USER_EMAIL_KEY';

  // saving data to shared_preference
  static Future<bool> saveUserLoggedIn({bool isUserLoggedIn}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('saving... $isUserLoggedIn');
    return await prefs.setBool(sharedPreferenceUserLoggedInKey, isUserLoggedIn);
  }

  static Future<bool> saveUserName({String username}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('saving... $username');
    return await prefs.setString(sharedPreferenceUserNameKey, username);
  }

  static Future<bool> saveUserEmail({String email}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('saving... $email');
    return await prefs.setString(sharedPreferenceUserEmailKey, email);
  }

  static Future<bool> saveMessageTimestamp({String chatroomId, int timestamp}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setInt(chatroomId, timestamp);
  }

  // get data from shared_preference
  static Future<bool> fetchUserLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(sharedPreferenceUserLoggedInKey);
  }

  static Future<String> fetchUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(sharedPreferenceUserNameKey);
  }

  static Future<String> fetchUserEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(sharedPreferenceUserEmailKey);
  }

  static Future<int> fetchMessageTimestamp({String chatroomId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(chatroomId);
  }

  static void deleteLoggedInData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('delete all local saved user info');
    prefs.remove(sharedPreferenceUserLoggedInKey);
    prefs.remove(sharedPreferenceUserNameKey);
    prefs.remove(sharedPreferenceUserEmailKey);
  }
}
